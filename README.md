# Composer template skeleton for Arizona Quickstart Experiment

This project template provides a starter kit for managing your site
dependencies with [Composer](https://getcomposer.org/). Clone this repository and: 

```
composer install
```

Webroot of the built Drupal 8 site file structure after composer is done should be located at `web/`.
